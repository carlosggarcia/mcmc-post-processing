#!/bin/bash

code=$HOME/codes/hi_class_pub_devel_emilio
level=background
derived="Omega_m H0"

chain=$HOME/codes/montepython_cgg_emilio/chains/cl_cross_corr_v4_blcdm/P18_mgclass_fs_lcdm_nolite_nonlinear/2021-11-29_10000000_

for i in {1..6}; do
	addqueue -n 1x2 -m 1 -s -q berg -c "newder $i ~1h" /usr/bin/python3 scripts/compute_new_derived.py ${chain}_$i.txt $level $derived;
done

# derived=M2_0_smg 
# 
# chain=$HOME/codes/montepython_upstream/chains/cccg_PBPl2/2020-06-14_1000000_
# i=1
# addqueue -n 1x2 -m 1 -s -q berg -c "newder $i ~1h" /usr/bin/python3 compute_new_derived.py $code ${chain}_$i.txt $level $derived;

