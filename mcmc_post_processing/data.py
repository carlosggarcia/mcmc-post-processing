#!/usr/bin/python
from glob import glob
from .chain import Chain
import numpy as np
import os

class Data():
    def __init__(self, path_chain_basename):
        self._basename = os.path.basename(path_chain_basename)
        self._inputdir = os.path.dirname(path_chain_basename)

        self.chains = self._load_chains()
        self.parameters = self.chains[0].parameters
        self.cosmo_arguments = self.chains[0].cosmo_arguments

        self.bf_chi2 = None
        self.bf_ix = None

    def _load_chains(self):
        """
        Read the chain to reprocess
        """
        fname = os.path.join(self._inputdir, self._basename)
        files = glob(fname + '*.txt')

        chains = tuple(Chain(fn) for fn in files)

        return chains

    def get_bestfit_ix(self):
        if self.bf_chi2 is None:
            bf_chi2 = np.ones(len(self.chains))
            for i, c in enumerate(self.chains):
                bf_chi2[i] = c.get_bestfit_chi2()
            self.bf_chi2 = bf_chi2

        if self.bf_ix is None:
            self.bf_ix = self.bf_chi2.argmin()

        return self.bf_ix

    def get_bestfit_chi2(self):
        ix = self.get_bestfit_ix()
        return self.bf_chi2[ix]

    def get_bestfit_step(self):
        ix = self.get_bestfit_ix()
        return self.chains[ix].get_bestfit_step()

    def get_bestfit_cosmo_pars(self):
        ix = self.get_bestfit_ix()
        return self.chains[ix].get_bestfit_cosmo_pars()
