#!/usr/bin/python
from collections import OrderedDict
import numpy as np
import os
import warnings
import sys

class Chain():
    def __init__(self, chain_path):
        self._basename = os.path.basename(chain_path)
        self._inputdir = os.path.dirname(chain_path)

        self.parameters, self.cosmo_arguments = self._read_MP_logparam()
        self.chain = np.loadtxt(chain_path)
        self.nsteps = self.chain.shape[0]

        self.bf_ix = None

    def _read_MP_logparam(self):
        """
        Read the log.param file in inputdir and return the parameters and
        cosmo_arguments set.
        """

        parameters = OrderedDict()
        cosmo_arguments = OrderedDict()

        fname = os.path.join(self._inputdir, 'log.param')
        with open(fname, 'r') as f:
            for line in f:
                if (not line.strip()) or ('#' == line.strip()[0]) or ('sBBN file' in line):
                    continue
                elif ('data.parameters' in line) or ('data.cosmo_arguments' in line):
                    exec(line.replace('data.', ''))

        return parameters, cosmo_arguments

    def step_to_cosmo_params(self, step, level=None):
        step = step[2:]  # Remove Ntrials, chi2 columns
        cosmo_params = {}
        par_list_tmp = []
        par_name_tmp = ''
        c = 0
        for par, pval in self.parameters.items():
            # Check the parameter is sampled. If so, advance the position index c
            if pval[3] != 0:
                val = step[c]
                c += 1
            else:
                val = pval[0]

            # If it is a nuisance parameter, we don't care
            if (pval[5] != 'cosmo'):
                continue

            scale = pval[4]
            val *= scale

            if '__' in par:
                name = par.split('__')[0]
                if name != par_name_tmp: # The last one will not change name. Add later
                    # This will store {'': []}. Remove it before return
                    cosmo_params[par_name_tmp] = ','.join(par_list_tmp)
                    par_list_tmp = []
                    par_name_tmp = name
                par_list_tmp.append(val)
            else:
                cosmo_params[par] = val

        cosmo_params[par_name_tmp] = str(par_list_tmp).strip('[]')
        del(cosmo_params[''])
        cosmo_params.update(self.get_cosmo_arguments(level=level))
        return cosmo_params

    def get_cosmo_arguments(self, level=None):
        cosmo_arguments = self.cosmo_arguments.copy()
        keys = cosmo_arguments.keys()
        if level == 'background':
            if 'output' in keys:
                del(cosmo_arguments['output'])
            if 'lensing' in keys:
                del(cosmo_arguments['lensing'])
            if 'P_k_max_h/Mpc' in keys:
                del(cosmo_arguments['P_k_max_h/Mpc'])
            if 'l_max_scalars' in keys:
                del(cosmo_arguments['l_max_scalars'])
            if 'non linear' in keys:
                del(cosmo_arguments['non linear'])
        elif level == 'perturbations':
            cosmo_arguments['output'] = 'mPk'
            if 'lensing' in keys:
                del(cosmo_arguments['lensing'])
            if 'P_k_max_h/Mpc' in keys:
                del(cosmo_arguments['P_k_max_h/Mpc'])
            if 'l_max_scalars' in keys:
                del(cosmo_arguments['l_max_scalars'])
            if 'non linear' in keys:
                del(cosmo_arguments['non linear'])
        elif level is not None:
            warnings.warn(f'{level} not implemented. Returning all.')

        return cosmo_arguments

    def get_bestfit_ix(self):
        if self.bf_ix is None:
            self.bf_ix = self.chain[:, 1].argmin()
        return self.bf_ix

    def get_bestfit_chi2(self):
        ix = self.get_bestfit_ix()
        return self.chain[ix, 1]

    def get_bestfit_step(self):
        ix = self.get_bestfit_ix()
        return self.chain[ix]

    def get_bestfit_cosmo_pars(self):
        ix = self.get_bestfit_ix()
        return self.step_to_cosmo_params(self.chain[ix])

    def get_step(self, ix):
        return self.chain[ix]

    def _write_paramnames(self, newderived, outdir):
        rname = self._basename.split('__')[0]
        fname = os.path.join(outdir, rname + '_.paramnames')
        with open(fname, 'w+') as f:
            for k, v in self.parameters.items():
                if (v[3] != 0) or (v[5] == 'derived'):
                    f.write(k + '\n')
            for k in newderived:
                f.write(k + '\n')

    def _save_steps(self, newstep, outdir):
        fname = os.path.join(outdir, self._basename)
        with open(fname, 'ab') as f:
            np.savetxt(f, newstep)

    def add_new_derived(self, newderived, level=None, istep=1,
                    outdir='./mcmc_postprocessed', steps_to_save=50):
        """
        newderived : list
        """
        from classy import Class
        from classy import CosmoComputationError
        os.makedirs(outdir, exist_ok=True)
        self._write_paramnames(newderived, outdir)
        cosmo = Class()

        nnew = len(newderived)
        nparams = self.chain.shape[1]

        steps_to_save = steps_to_save
        newstep = -1 * np.ones((steps_to_save, nparams + nnew))

        c = 0
        pars_old = {}
        for i in range(istep - 1, self.nsteps):
            print("Computing step {} / {}".format(i+1, self.nsteps))
            pars = self.step_to_cosmo_params(self.chain[i], level=level)
            if 'sBBN file' in pars:
                del(pars['sBBN file'])
            if pars != pars_old:
                print('Using hi_class')
                pars_old.update(pars)
                cosmo.set(pars)
                try:
                    cosmo.compute()
                    der_dic = cosmo.get_current_derived_parameters(newderived)
                    derived = np.array([der_dic[p] for p in newderived])
                except CosmoComputationError as e:
                    derived = np.nan * np.ones(nnew)
                    sys.stderr.write("------------------------- Error ---------------------\n")
                    sys.stderr.write("Failed step {} for parameters:\n {}\n ".format(i+1, str(cosmo.pars)))
                    sys.stderr.write("Failed step {} with error message: {}\n".format(i+1, str(e)))
                    sys.stderr.write("-----------------------------------------------------\n")
                cosmo.struct_cleanup()
            newstep[c, :nparams] = self.chain[i]
            newstep[c, nparams:] = derived
            c += 1

            if c == steps_to_save:
                self._save_steps(newstep, outdir)
                newstep = -1 * np.ones((steps_to_save, nparams + nnew))
                c = 0

        if c != 0:
            self._save_steps(newstep[:c], outdir)
