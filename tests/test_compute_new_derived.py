#!/usr/bin/python

import os
import numpy as np
import shutil

def test_compute_new_derived():
    ichain = 'tests/data/h_hst/2020-12-22_1000__1.txt'
    chain_name = os.path.basename(ichain)

    outdir = 'tests/data/compute_new_derived_test'
    ochain = os.path.join(outdir, chain_name)
    if os.path.isdir(outdir):
        shutil.rmtree(outdir)

    checkdir = 'tests/data/h_hst_Omega_Lambda/'
    check_chain = os.path.join(checkdir, chain_name)

    os.system(f'python scripts/compute_new_derived.py --outdir {outdir} {ichain} background Omega_Lambda')

    c0 = np.loadtxt(ochain)
    c1 = np.loadtxt(check_chain)

    assert np.max(np.abs(c1 / c0 - 1)) < 1e-5
