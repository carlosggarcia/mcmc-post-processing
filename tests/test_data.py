#!/usr/bin/python

import os
import numpy as np
import sys
sys.path.append('./')
from mcmc_post_processing.data import Data
from mcmc_post_processing.chain import Chain


path_chain_basename = 'tests/data/h_hst_Omega_Lambda/2020-12-22_1000_'
mp_folder = os.path.dirname(path_chain_basename)

data = Data(path_chain_basename)
np_chain1 = np.loadtxt(path_chain_basename + '_1.txt')
np_chain2 = np.loadtxt(path_chain_basename + '_2.txt')

chain1 = Chain(path_chain_basename + '_1.txt')
chain2 = Chain(path_chain_basename + '_2.txt')

chi2_1_bf = np.min(np_chain1[:, 1])
chi2_2_bf = np.min(np_chain2[:, 1])

chains = (chain1, chain2)
np_chains = (np_chain1, np_chain2)
chi2_bf = np.array((chi2_1_bf, chi2_2_bf))

# def test_load_chains():
#     assert len(data.chains) == len(chains)

def test_get_bestfit_ix():
    assert np.argmin(chi2_bf) == data.get_bestfit_ix()

def test_get_bestfit_chi2():
    assert np.min(chi2_bf) == data.get_bestfit_chi2()

def test_get_bestfit_step():
    ix = np.argmin(chi2_bf)
    ixc = np.argmin(np_chains[ix][:, 1])
    assert np.all(np_chains[ix][ixc] == data.get_bestfit_step())

def test_get_bestfit_cosmo_pars():
    ix = np.argmin(chi2_bf)
    assert chains[ix].get_bestfit_cosmo_pars() == data.get_bestfit_cosmo_pars()






