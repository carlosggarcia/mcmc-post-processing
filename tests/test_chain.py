#!/usr/bin/python

import os
import numpy as np
import sys
sys.path.append('./')
from mcmc_post_processing.chain import Chain


ichain = 'tests/data/h_hst_Omega_Lambda/2020-12-22_1000__1.txt'
chain_name = os.path.basename(ichain)

chain = Chain(ichain)
np_chain = np.loadtxt(ichain)

def test_step_to_cosmo_params():
    pass

def test_get_bestfit():
    ix = np.argmin(np_chain[:, 1])
    assert ix == chain.get_bestfit_ix()

def test_get_bestfit_chi2():
    assert np_chain[:, 1].min() == chain.get_bestfit_chi2()

def test_get_bestfit_step():
    ix = np.argmin(np_chain[:, 1])
    assert np.all(np_chain[ix] == chain.get_bestfit_step())

def test_get_bestfit_cosmo_pars():
    from classy import Class
    pars = chain.get_bestfit_cosmo_pars()
    step = chain.get_bestfit_step()

    cosmo =  Class()
    cosmo.set(pars)
    cosmo.compute()

    Omega_Lambda = cosmo.get_current_derived_parameters(['Omega_Lambda'])['Omega_Lambda']

    assert np.max(np.abs(step[-1] / Omega_Lambda - 1)) < 1e-5


