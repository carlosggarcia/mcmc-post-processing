#!/usr/bin/python
"""
Compute new derived parameters for MontePython chains.
"""

if __name__ == "__main__":
    import sys
    sys.path.append('./')
    from mcmc_post_processing.chain import Chain
    import argparse

    #### Parser
    parser = argparse.ArgumentParser(description="Compute new derived parameters for MontePython chains")
    parser.add_argument('--outdir', default='./mcmc_postprocessed',type=str, help='Output directory')
    parser.add_argument('--istep', default='1', type=int, help='Initial step (starting on 1)')
    parser.add_argument('chain', default='None',  type=str, help='Chain to\
                        postprocess. Must be in a MP output folder')
    parser.add_argument('level', type=str, help='Level of computation (background, lensing, ...)')
    parser.add_argument('newderived', type=str, nargs='+', help='Derived parameters to compute')
    args = parser.parse_args()
    ####

    chain = Chain(args.chain)
    chain.add_new_derived(args.newderived, args.level, args.istep, outdir=args.outdir)
